package com.oopuniversity.reddit.javahelp;

import java.util.Scanner;

public class MidInteger {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);

        int[] userValues = new int[9];
        int num = 0, cnt = 0;

        while ((num = scnr.nextInt()) > 0 && cnt < 9) {
            userValues[cnt++] = num;
        }
        System.out.println(userValues[cnt/2]);
    }

}
