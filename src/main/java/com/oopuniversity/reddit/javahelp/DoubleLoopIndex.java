package com.oopuniversity.reddit.javahelp;

public class DoubleLoopIndex {
    public static void main(String[] args) {
        for (int i = 0, j = 40, k=5; i < 15 && j < 55; i+=2, j--) {
            if (i % 3 == 0) {
                j=Math.abs(j);
                j-=3;
            }
            k = k + j * 2;
            System.out.println("i=" + i + ", j=" + j + ", k = " + k);
        }

    }
}
