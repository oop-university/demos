package com.oopuniversity.roman;

public class RomanNumerals {

    static String repeat(String value, int n) {
        StringBuffer sb = new StringBuffer();
        for(int i = 0; i < n; i++) {
            sb.append(value);
        }
        return sb.toString();
    }
    public static String asRoman(int number) {
        return String.join("",
                    repeat("I", number))
                .replace("IIIII", "V")
                .replace("IIII", "IV")
                .replace("VIV", "IX")
                .replace("VV", "X")
                .replace("XXXXX", "L")
                .replace("XXXX", "XL")
                .replace("XLIX", "IL")
                .replace("LL", "C")
                .replace("LXL", "XC")
                .replace("LIL", "IC")
                .replace("CCCCC", "D")
                .replace("CCCC", "CD")
                .replace("DD", "M")
                .replace("DCD", "CM")
                .replace("CDIC", "ID")
                .replace("CMIC", "IM")
                ;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 2021; i++) {
            System.out.printf("%6d - %s\n", i, asRoman(i));
        }
    }
}
