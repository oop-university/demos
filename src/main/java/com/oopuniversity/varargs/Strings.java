package com.oopuniversity.varargs;

public class Strings {
    static public String asCsv(String... item) {
        return String.join(", ", item);
    }
}
