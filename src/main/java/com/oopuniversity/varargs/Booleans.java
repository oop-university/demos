package com.oopuniversity.varargs;

import java.util.Arrays;

public class Booleans {
    static public boolean noFalsesFound(Boolean... b) {
        return Arrays.stream(b).allMatch(c -> c);
    }
}
