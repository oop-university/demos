package com.oopuniversity.varargs;

import java.util.Arrays;

import static com.oopuniversity.varargs.Booleans.noFalsesFound;
import static com.oopuniversity.varargs.Strings.asCsv;

public class VarArgs {
    public static void main(String[] args) {
        Boolean[] booleans = new Boolean[0];
        displayBooleans();
        displayBooleans(booleans);
        displayBooleans(false);
        displayBooleans(true);
        displayBooleans(true, true);
        displayBooleans(false, false);
        displayBooleans(false, true);
        displayBooleans(true, false);
        displayBooleans(true, true, true, true, true, true, true, false);
        displayBooleans(true, true, true, true, true, true, true, true);

        System.out.println(asCsv());
        System.out.println(asCsv("one"));
        System.out.println(asCsv("Bob", "Dick", "Harry"));
    }

    public static void displayBooleans(Boolean... booleans) {
        System.out.println(Arrays.toString(booleans) + ": " + noFalsesFound(booleans));
    }


}
