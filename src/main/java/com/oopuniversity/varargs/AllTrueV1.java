package com.oopuniversity.varargs;

public class AllTrueV1 {
    public static boolean allTrue(Boolean... b) {
        for (boolean bool : b) {
            if (!bool) return false;
        }
        return true;
    }
}

