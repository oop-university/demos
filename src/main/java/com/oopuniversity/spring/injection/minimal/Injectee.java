package com.oopuniversity.spring.injection.minimal;

import com.oopuniversity.spring.injection.minimal.Injected;
import org.springframework.stereotype.Component;

/**
 * Created by OOP University on 6/5/2016.
 */
@Component
public class Injectee {

    private final Injected injected;

    public Injectee(Injected injected) {
        this.injected = injected;
    }

    public void doSomething() {
        System.out.println(toString());
        System.out.println(injected.toString());
    }

    public String toString() {
        return "I am Injectee, an automatically instantiated object.";
    }
}
