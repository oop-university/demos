package com.oopuniversity.functional.controller;

import com.oopuniversity.functional.discount_service.DiscountService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DiscountController {
    DiscountService discountService;

    @GetMapping("/discount/{points}")
    public DiscountReturn getDiscountCodeForPoints(@PathVariable int points) {
        return new DiscountReturn(points, discountService.calculateDiscount(points));
    }

    @AllArgsConstructor
    public static final class DiscountReturn {
        public int points;
        public int discount;
    }

}
