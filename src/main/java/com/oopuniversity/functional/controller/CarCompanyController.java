package com.oopuniversity.functional.controller;

import com.oopuniversity.functional.optional.CarCompany;
import com.oopuniversity.functional.optional.CarCompanyRepo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@Slf4j
@AllArgsConstructor
@RestController
public class CarCompanyController {
    @GetMapping("/")
    public List<String> examples() {
        return Arrays.asList(
                "/carmakers/{companyName}",
                "/carmakers/{companyName}/models",
                "/discount/{points}"
        );
    }

    @GetMapping("/carmakers/{companyName}/models")
    public List<String> getModelsForCarCompanyByName(@PathVariable String companyName) {
        return CarCompanyRepo.getCarCompany(companyName).map(CarCompany::getModels).orElse(null);
    }

    @GetMapping("/carmakers/{companyName}")
    public CarCompany getCarCompanyByName(@PathVariable String companyName) {
        return CarCompanyRepo.getCarCompany(companyName).orElse(null); //
    }
}
