package com.oopuniversity.functional;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ExampleObject {
    @JsonProperty("lastName")
    public String lastName = "Luke";
    public String getLastName() {
        return lastName;
    }
}
