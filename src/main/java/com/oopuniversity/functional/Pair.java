package com.oopuniversity.functional;

public class Pair {
    private final String key;
    private final String value;

    @Override
    public String toString() {
        return String.join("=", key, value);
    }

    public Pair(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
