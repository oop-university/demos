package com.oopuniversity.functional.optional;

import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Slf4j
public class AvoidNullCheck {

    public static void main(String[] args) {
        new AvoidNullCheck().testDrive();
    }

    /*
     *Note that no matter what I feed this method,
     *I do not have to worry much about null pointers.
     */

    void displayCarCompany(String companyName) {

        System.out.println("Company information for " + companyName);

        /*
         * Optional is a wrapper.  It may or may not have an object
         * inside of it, but you will never get a null pointer from
         * references to 'maybeCompany'.  In a sense it's a lot
         * like iterating over a list...  (and yes you can apply
         * all of this stuff to doing that as well) in that if
         * the List has no elements, you can still iterate zero
         * times and not worry overmuch about referring to an
         * element that is not there.
         */
        Optional<CarCompany> maybeCompany = CarCompanyRepo.getCarCompany(companyName);
        System.out.println("Return from getCarCompany: " + maybeCompany);

        /*
         * Note that have a simple transformation here.
         * 'asJson' is a utility method that turns a
         * CarCompany (or any object, really) into a
         * JSON-formatted String.  The key thing to
         * grok is that the method invoked inside of
         * map is used to take the 'current' stream
         * object and turn it into something else.  Oh,
         * and that if the result is Optional.empty()
         * no further processing of the chain will
         * happen,
         *
         * In this case, all we're doing is using a
         * simple transform (via the Jackson ObjectMapper)
         * to turn an Object into a JSON-formatted String.
         *
         * ifPresent gives us our hook to implement some
         * sort of behavior on the final result, if any.
         * In the interest of simplicity here I'm just
         * spitting out the results to standard output.
         */
        maybeCompany
                .map(JsonGenerator::asJson)
                .ifPresent(System.out::println);
        /*
         * Here we go through one additional transformation,
         * using the 'getModels' method of CarCompany to
         * change the type of the stream from Optional<CarCompany>
         * to Optional<List<String>>.  If maybeCompany was
         * empty(), of course, this will be bypassed, avoiding
         * any need to check for a null result.
         *
         */
        maybeCompany
                .map(CarCompany::getModels)
                .map(JsonGenerator::asJson)
                .ifPresent(System.out::println);
    }

    void testDrive() {
        displayCarCompany("Ford");
        displayCarCompany("Chevrolet");
        displayCarCompany("Chevorlet");
        displayCarCompany(null);
    }
}
