package com.oopuniversity.functional.optional;

import java.util.*;

public class CarCompanyRepo {
    static Map<String, CarCompany> carCompanyMap;/*
    Note that if there's no Car the optional will say OK, nothing to return here
    and it will remain empty.  But you *always* get an Optional back, so there
    is no need to ask if the return from getCarCompany is null
    */
    static {
        carCompanyMap = new HashMap<>();
        List<String> chevrolets = Arrays.asList("Impala", "Crown Victoria");
        List<String> fords = Arrays.asList("Mustang", "Explorer");
        carCompanyMap.put("Ford",
                new CarCompany("Ford", "Joe Ford", 1907, fords));
        carCompanyMap.
                put("Chevrolet", new CarCompany("Chevrolet", "Bill Chevrolet", 1925, chevrolets));
    }

    static public Optional<CarCompany> getCarCompany(String id) {
        return Optional.ofNullable(carCompanyMap.get(id));
    }
}