package com.oopuniversity.functional.optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

public class JsonGenerator {
    @SneakyThrows
    public static String asJson(Object value) {
        System.out.println("asJson(" + value + ")");
        return new ObjectMapper().writeValueAsString(value);
    }
}