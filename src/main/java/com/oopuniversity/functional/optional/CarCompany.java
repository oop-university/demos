package com.oopuniversity.functional.optional;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

@Slf4j
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@Getter
public class CarCompany {
    private final String name;
    private final String CEO;
    private final Integer foundingYear;
    private final List<String> models;

}
