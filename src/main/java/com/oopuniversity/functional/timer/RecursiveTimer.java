package com.oopuniversity.functional.timer;

import lombok.SneakyThrows;

import java.util.Optional;

import static java.util.Optional.ofNullable;

public class RecursiveTimer {
    private RecursiveTimer parent;
    private long delay;

    private RecursiveTimer(RecursiveTimer parent, long delay) {
        this.delay = delay;
        this.parent = parent;
    }

    public static RecursiveTimer create(RecursiveTimer parent, long delay) {
        return new RecursiveTimer(parent, delay);
    }

    Optional<RecursiveTimer> getParent() {
        return ofNullable(parent);
    }

    @SneakyThrows
    public Long sleep() {
        Thread.sleep(delay);
        return getParent().map(RecursiveTimer::sleep).orElse(0L);
    }
}
