package com.oopuniversity.functional.timer;

import lombok.extern.slf4j.Slf4j;

import static java.lang.System.out;

@Slf4j
public class TimeIt {
    void operation1() {
        out.println("o1");
    }

    String operation2() {
        out.println("o2");
        return "o2 return value";
    }

    void operation3(int p1) {
        out.println("o3: " + p1);
    }

    public static void main(String[] args) {
        TimeIt timeIt = new TimeIt();
        String s;
        timeIt.time(() -> timeIt.operation1());
        s = timeIt.time(() -> timeIt.operation2());
        timeIt.time(() -> timeIt.operation3(5));
    }

    private final String format;

    public TimeIt(String newFormat) {
        this.format = newFormat;
    }

    public TimeIt() {
        this.format = "Execution time {} ns";
    }

    public <R> R time(Runnable block) {
        long start = System.nanoTime();
        try {
            block.run();
        } finally {
            long end = System.nanoTime();
            log.info(format, (end - start)/* / 1.0e9 */);
        }
        return null;
    }
}
