package com.oopuniversity.functional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FunctionalDemo {

    public static void main(String[] args) {
        new SpringApplication(FunctionalDemo.class)
                .run(args);
    }
}
