package com.oopuniversity.functional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class BifunctionExploration {

    public static void main(String[] args) {
        List<String> x = Arrays.asList("foo", "bar", "baz");
        List<String> one = Arrays.asList("one", "two");
        List<String> two = Arrays.asList("and a", "chickaboom");

        List<String> woven = x.stream()
                .flatMap(partial -> weave(partial, one))
                .flatMap(first -> weave(first, two)) //Bifunction
                .collect(toList());
        woven.forEach(System.out::println);
    }

    private static Stream<String> weave(String first, List<String> two) {
        return two.stream()
                .map(second -> join(first, second));
    }

    private static String join(String one, String two) {
        return one + " " + two;
    }
}
