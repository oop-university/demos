package com.oopuniversity.functional.traversal;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.Map.Entry;

public class TreeWalk {
    /**
     * Given any object, call the provided consumer on it and
     * then attempt to identify any subtrees within it and
     * call walk again on each.
     *
     * @param object Any object, which we will try to treat as a root element
     * @param processor A function which will be called against the root element,
     *                  and which can optionally transform the object before the
     *                  next processing step.
     */
    public static void walk(Object object, Function<Object, Object> processor) {
        asStream(object)
                .map(value -> processor.apply(value))
                .forEach(submap -> walk(getChildren(submap), processor));
    }

    /**
     * Convert an Object into a set of zero or more child
     * objects.
     *
     * @param value
     * @return
     */
    private static Object getChildren(Object value) {
        if (value instanceof Map) {
            return ((Map) value).values();
        } else if (value instanceof Entry) {
            return ((Entry) value).getValue();
        } else if (value instanceof Collection) {
            return value;
        } else {
            return null;
        }
    }

    /**
     * Stream accepts any Object, and attempts to create
     * a Stream from it.
     * If object is a Map, the EntrySet will be streamed
     * If object is a Collection, object will be streamed
     * If object is null, returns an empty stream
     * Otherwise, return Stream.of(object)
     *
     * @param object
     * @return
     */
    private static Stream asStream(Object object) {
        if (object instanceof Map) {
            return ((Map) object).entrySet().stream();
        } else if (object instanceof Collection) {
            return ((Collection) object).stream();
        }
        return Stream.ofNullable(object);
    }
}
