package com.oopuniversity.functional;

import java.util.Optional;

import static java.util.Optional.of;

public class FunctionalMap {
    final FunctionalMap base;
    final Pair pair;

    public FunctionalMap(FunctionalMap basedOn, Pair pair) {
        this.pair = pair;
        this.base = basedOn;
    }

    @Override
    public String toString() {
        return (null == base) ?
                pair.toString() :
                String.join(",",
                        base.toString(),
                        pair.toString());
    }

    public static void main(String[] args) {
        FunctionalMap functionalMap = new FunctionalMap(null, new Pair("a", "b"));//new FunctionalMap();
        FunctionalMap fm2 = new FunctionalMap(functionalMap, new Pair("b", "c"));
        FunctionalMap fm3 = new FunctionalMap(fm2, new Pair("hells", "bells"));
        System.out.println(functionalMap.toString());
        System.out.println(functionalMap.get("b"));
        System.out.println(fm3.toString());
        System.out.println(fm3.get("b"));
    }

    private String get(String key) {
        if (pair.getKey().equals(key)) {
            return pair.getValue();
        } else {
            return null == base
                    ? ""
                    : base.get(key);
        }
    }

}
