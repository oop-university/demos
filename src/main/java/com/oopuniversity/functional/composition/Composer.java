package com.oopuniversity.functional.composition;

import java.util.function.Function;

public class Composer {
    static Function<Integer, Integer> TimesTwo = e -> e * 2;
    static Function<Integer, Integer> PlusEight = e -> e + 8;
    static Function<Integer, Integer> MinusThree = e -> e - 3;
    static Function<Integer, Integer> Squared = e -> e * e;
    static Function<Integer, Integer> TimesTwoPlusEight = TimesTwo.andThen(PlusEight);

    static Integer invoke(Integer in, Function<Integer, Integer> func) {
        return func.apply(in);
    }


    public static void main(String[] args) {
        System.out.println(invoke(15, TimesTwo));
        System.out.println(invoke(15, MinusThree));
        System.out.println(invoke(15, PlusEight));

        System.out.println(invoke(15, Squared));

        System.out.println(invoke(15, MinusThree.andThen(Squared)));
        System.out.println(invoke(15, Squared.andThen(MinusThree)));

        System.out.println(invoke(15, TimesTwoPlusEight.andThen(MinusThree).andThen(Squared)));
    }
}
