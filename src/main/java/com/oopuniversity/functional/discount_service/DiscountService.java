package com.oopuniversity.functional.discount_service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Component
public class DiscountService {
    final List<RangeDefinition> discounts = Arrays.asList(
            new RangeDefinition(0, 5, 0),
            new RangeDefinition(5, 20, 3),
            new RangeDefinition(20, 1000, 10));

    public Integer calculateDiscount(int points) {
        return discounts.stream()
                .filter(key -> key.isInRange(points))
                .findFirst()
                .map(k -> k.value)
                .orElseGet(() -> outOfRangeResponse(points));
    }

    private Integer outOfRangeResponse(int fromGiven) {
        log.error("An undefined value of " + fromGiven + " was requested.  Supplying default.");
        return fromGiven < 0 ? 0 : 10;
    }

    public String showDiscount(int points) {
        return "Value: " + calculateDiscount(points);
    }

    @AllArgsConstructor
    private static class RangeDefinition {
        public final int lowBound;
        public final int highBound;
        public final int value;

        Boolean isInRange(Integer testValue) {
            return testValue >= lowBound && testValue < highBound;
        }
    }
}
