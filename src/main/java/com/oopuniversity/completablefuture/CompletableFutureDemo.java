package com.oopuniversity.completablefuture;


import lombok.SneakyThrows;

import java.io.IOException;
import java.net.URI;
//import java.net.http.HttpRequest;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

//import static java.net.http.HttpClient.newHttpClient;
//import static java.net.http.HttpResponse.BodyHandlers.ofString;

public class CompletableFutureDemo {

//    static List<CompletableFuture<WebSite>> createReaders(String... url) {
//        return Arrays.stream(url)
//                .map(CompletableFutureDemo::createReader)
//                .collect(Collectors.toList());
//    }

//    static CompletableFuture<WebSite> createReader(String url) {
//        return makeSiteReader(url)
//                .thenApply(CompletableFutureDemo::logCompletion);
//    }

    private static WebSite logCompletion(WebSite webSite) {
        System.out.println("Completed: " + webSite);
        return webSite;
    }

    private static final int initialDelay = 250;

//    public static void main(String[] args) {
//        List siteReaders = startSiteReaders(args);
//        waitFor(siteReaders, initialDelay);
//        System.out.println("All done I guess");
//    }

    static void waitFor(List siteReaders, long delay) {
        if (RunCounter.anyStillRunning(siteReaders.stream())) {
            sleep(delay);
            System.out.print(".");
            waitFor(siteReaders, delay * 2);
        }
    }


    @SneakyThrows
    private static void sleep(Long millis) {
        Thread.sleep(millis);
    }

//    private static List<CompletableFuture> startSiteReaders(String... urls) {
//        return createReaders(urls).stream()
//                .map(siteReader -> siteReader
//                        .thenAccept(WebSite::displaySiteInfo))
//                .collect(Collectors.toList());
//    }

//    private static CompletableFuture<WebSite> makeSiteReader(String url) {
//        return CompletableFuture
//                .supplyAsync(() -> readWebPage(url))
//                .exceptionally(e -> new WebSite(url, e.toString(), ""))
//                ;
//    }

//    @SneakyThrows
//    private static WebSite readWebPage(String url) {
//        return new WebSite(url,
//                "OK", asSiteContents(url));
//    }

//    private static String asSiteContents(String url) throws IOException, InterruptedException {
//        return newHttpClient()
//                .send(asHttpRequest(url), ofString())
//                .body();
//    }
//
//    private static HttpRequest asHttpRequest(String url) {
//        return HttpRequest.newBuilder()
//                .uri(URI.create(url))
//                .build();
//    }

}