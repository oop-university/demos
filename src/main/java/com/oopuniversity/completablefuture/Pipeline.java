package com.oopuniversity.completablefuture;

import com.oopuniversity.functional.logging.Logger;

import java.util.concurrent.CompletableFuture;

public class Pipeline {
    public static void main(String[] args) {
        Pipeline pipeline = new Pipeline();
        pipeline.run();
    }

    public void run() {
        CompletableFuture<Double> future = new CompletableFuture<>();
        future.thenApply(data -> data * 3.7)
                .thenApply(Logger::logging)
                .thenApply(data -> data / 2.0)
                .thenApply(Logger::logging)
                .exceptionally(Pipeline::error)
                .thenAccept(Pipeline::reportFinal);

        future.complete(5.32110);
    }

    private static void reportFinal(Double aDouble) {
        System.out.println("Done!  " + aDouble);
    }

    private static Double error(Throwable throwable) {
        System.out.println("Double error!  Supplying zero.");
        return 0d;
    }
}
