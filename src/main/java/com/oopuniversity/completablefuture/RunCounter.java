package com.oopuniversity.completablefuture;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class RunCounter {
    public static long numberStillRunning(Stream<CompletableFuture> futureStream) {
        return futureStream
                .filter(s -> !s.isDone())
                .count();
    }

    public static boolean anyStillRunning(Stream<CompletableFuture> futureStream) {
        return futureStream
                .anyMatch(s -> !s.isDone());
    }

}