package com.oopuniversity.completablefuture;

class WebSite {
    public static void displaySiteInfo(WebSite s) {
        System.out.println("Displaying: " + s.url + " (" + s.body.length() + " characters)" + s.message);
    }

    @Override
    public String toString() {
        return "WebSite{" +
                "url='" + url + '\'' +
                ", message='" + message + '\'' +
                ", body is " + body.length() + " characters" +
                '}';
    }

    private final String url;
    private final String message;
    private final String body;

    public WebSite(String url, String message, String body) {
        this.url = url;
        this.message = message;
        this.body = body;
    }
}
