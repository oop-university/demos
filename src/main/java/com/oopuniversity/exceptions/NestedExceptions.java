package com.oopuniversity.exceptions;

/**
 * I was given a merge request with structure much like this
 * and at first I objected...  But when I stopped to think about
 * it for a bit I realized that it was actually a really nice way
 * to handle the specific situation we were dealing with
 */
public class NestedExceptions {
    public static void main(String[] args) {
        NestedExceptions nestedExceptions = new NestedExceptions();
        nestedExceptions.testNestedExceptions();
    }

    void testNestedExceptions() {
        System.out.println("1");
        try {
            System.out.println("2");
            try {
                System.out.println("3");
                String s;
                s = null;
                s.notify();
                System.out.println("3.5");
            } finally {
                System.out.println("4");
            }
        } catch (Exception e) {
            System.out.println("5");
        } finally {
            System.out.println("6");
        }
        System.out.println("7");
    }
}
