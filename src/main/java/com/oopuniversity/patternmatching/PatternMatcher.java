package com.oopuniversity.patternmatching;

public class PatternMatcher {
    public static void main(String[] args) {
        PatternMatcher patternMatcher = new PatternMatcher();
        patternMatcher.go();
    }

    private void go() {
        String s = "Hello, patterns!";

        Object o = s;

        if (o instanceof String) {
            String s2 = (String) o;
            System.out.println(s2);
        }

    }
//We'll revisit this when a new pipeline version can be used
//    private Node TypeSwitcher(Node n) {
//        return switch(n) {
//            case NavigationNode(Node n) ->  n;
//            default -> null;//"Unknown";
//        };
//    }

}
