package com.oopuniversity.patternmatching;

public class NavigationNode implements Node {
    private final Node left;
    private final Node right;
    public NavigationNode(Node self) {
        this.left = self;
        this.right = null;
    }

    public NavigationNode(Node left, Node right) {
        this.left = left;
        this.right = right;
    }
}
