package com.oopuniversity.completablefuture;

import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class RunCounterTest {

    @Test
    void zeronumberStillRunning() {
        CompletableFuture<Void> cf = new CompletableFuture<>();
        cf.complete(null);
        assertEquals(0, RunCounter.numberStillRunning(Stream.of(cf)));
    }

    @Test
    void onenumberStillRunning() {
        CompletableFuture<Void> cf = new CompletableFuture<>();
        assertEquals(1, RunCounter.numberStillRunning(Stream.of(cf)));
    }

    @Test
    void trueanyStillRunning() {
        CompletableFuture<Void> cf = new CompletableFuture<>();
        assertTrue(RunCounter.anyStillRunning(Stream.of(cf)));
    }
    @Test
    void falseanyStillRunning() {
        CompletableFuture<Void> cf = new CompletableFuture<>();
        cf.complete(null);
        assertFalse(RunCounter.anyStillRunning(Stream.of(cf)));
    }
}