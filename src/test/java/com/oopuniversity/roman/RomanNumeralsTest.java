package com.oopuniversity.roman;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static com.oopuniversity.roman.RomanNumerals.asRoman;
import static org.junit.jupiter.api.Assertions.*;

class RomanNumeralsTest {
//This was all perfectly cromulent under Java 11, but Java 8 is primitive by comparison.
    static Map<Integer, String> expectations = Map.of(0, "",
            1, "I",
            2, "II",
            3, "III",
            4, "IV",
            5, "V",
            6, "VI",
            9, "IX",
            10, "X"
    );


    @Test
    void asRomanZero() {
        expectations.entrySet().stream()
                .forEach(testCase -> assertEquals(testCase.getValue(), asRoman(testCase.getKey())));
    }

}