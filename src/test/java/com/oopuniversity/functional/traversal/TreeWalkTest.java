package com.oopuniversity.functional.traversal;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.Map.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class TreeWalkTest {
    private static final Logger log = LoggerFactory.getLogger(TreeWalkTest.class);

    @Test
    void fullMapStringAccumulatingTransformer_findsAllBranches() {
        log.info("Calling walk with a list building transformer.");
        List<String> list = new ArrayList<>();
        TreeWalk.walk(buildDemoMap(), s -> accumulateStrings(list, s));
        log.info("Resulting list: {}", list);
        assertEquals(
                8,
                list.size());
    }

    @Test
    void fullMapMapAccumulatingTransformer_findsAllBranches() {
        log.info("Calling walk with a map building transformer.");
        Map<String, String> map = new HashMap<>();
        TreeWalk.walk(buildDemoMap(), s -> accumulateMapsAsOneMap(map, s));
        log.info("Resulting map: {}", map);
        assertEquals(
                9,
                map.size());
    }

    private Object accumulateMapsAsOneMap(Map map, Object s) {
        if (s instanceof Map.Entry) {
            addEntry(map, (Map.Entry) s);
        }
        return s;
    }

    private void addEntry(Map map, Map.Entry entry) {
        map.put(entry.getKey(), entry.getValue());
    }

    @Test
    void Walk_visitsEveryNodeOfAComplexMap() {
        log.info("Calling walk with a counting transformer.");
        Counter counter = new Counter();

        TreeWalk.walk(buildDemoMap(), counter);

        assertEquals(17, counter.getCount());
    }

    @Test
    void fullMapNullTransformer_DoesntDoAnythingUgly() {

        Map map = buildDemoMap();
        log.info("Calling walk with a null transformer.");

        TreeWalk.walk(map, s -> s);

        assertTrue(true, "Succeeded.");
    }

    private Object accumulateStrings(List<String> list, Object s) {
        if (s instanceof String) {
            list.add((String) s);
        }
        return s;
    }


    Map buildDemoMap() {
        Map map = new HashMap<>();
        Map sm1 = new HashMap();
        sm1.put("sm1.v1", "sm1.v1 value1");
        Map ms1 = new HashMap();
        ms1.put("ms1.v1", "ms1.v1 1");
        ms1.put("ms1.v2", "ms1.v2 2");
        Map sm2 = of("sm2.ms1", ms1, "sm2.v1", "value2", "sm2.v2", "value3");
        map.put("sm1", sm1);
        map.put("sm2", sm2);
        map.put("lm1", List.of("One", "Two", "Three"));
        return map;
    }

    private static class Counter implements Function<Object, Object> {

        private long value = 0;

        @Override
        public Object apply(Object o) {
            value++;
            return o;
        }

        public long getCount() {
            return value;
        }
    }
}