package com.oopuniversity.functional.consumer;

import com.oopuniversity.functional.discount_service.DiscountService;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DiscountServiceTest {

    @Test
    void shouldLogAnErrorAndReturnAZeroForNegativePointsValue() {
        assertEquals("Value: 0", new DiscountService().showDiscount(-1));
    }

    @Test
    void shouldReturnZeroWhenCustomerHasZeroToFourPoints() {
        DiscountService discountService = new DiscountService();
        IntStream.range(0, 5). //Note that the second parameter is exclusive, and the range goes only to four
                forEach(points -> assertEquals("Value: 0", discountService.showDiscount(points)));
    }

    @Test
    void shouldReturnThreeWhenCustomerHas5To19Points() {
        DiscountService discountService = new DiscountService();
        IntStream.range(5, 20).
                forEach(points -> assertEquals("Value: 3", discountService.showDiscount(points)));
    }

    @Test
    void shouldReturnTenWhenCustomerHas20To1000Points() {
        DiscountService discountService = new DiscountService();
        IntStream.range(20, 999).
                forEach(points -> assertEquals("Value: 10", discountService.showDiscount(points)));
    }

    @Test
    void shouldDisplayErrorLogAndReturnZeroForPointsOver1000() {
        assertEquals("Value: 10", new DiscountService().showDiscount(1001));

    }
}

